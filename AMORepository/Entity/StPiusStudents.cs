﻿using System;
namespace AMORepository.Entity
{
    public class StPiusStudents
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string DateOfGraduation { get; set; }
    }
}
