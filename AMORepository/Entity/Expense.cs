﻿using System;
namespace AMORepository.Entity
{
    public class Expense
    {
        public long Id { get; set; }

        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public decimal Amount { get; set; }

        public string ExpenseDetails { get; set; }

        public string ApprovedBy { get; set; }
    }
}
