﻿using System;
namespace AMORepository.Entity
{
    public class Employee
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public DateTime DOJ { get; set; }
        public string AdharNumber { get; set; }
        public string PanNumber { get; set; }
        public string Address { get; set; }
    }
}
