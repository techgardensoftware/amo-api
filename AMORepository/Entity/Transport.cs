﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AMORepository.Entity
{
    public class Transport
    {
        public long Id { get; set; }

        public string ModeOfTransport { get; set; }

        public DateTime AppliedDate { get; set; }

        public decimal MinAmount { get; set; }

        public double Distance { get; set; }

        public string Status { get; set; }
    }
}
