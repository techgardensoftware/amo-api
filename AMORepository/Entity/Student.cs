﻿using System;
namespace AMORepository.Entity
{
    public class Student
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Class { get; set; }
        public string AdharNumber { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
