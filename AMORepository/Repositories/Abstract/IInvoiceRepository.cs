﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AMORepository.Entity;
using AMORepository.Generic;

namespace AMORepository.Repositories.Abstract
{
    public interface IInvoiceRepository : IGenericRepository<Invoice>
    {
    }
}
