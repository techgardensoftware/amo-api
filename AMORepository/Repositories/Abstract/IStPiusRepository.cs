﻿using System;
using AMORepository.Entity;
using AMORepository.Generic;

namespace AMORepository.Repositories.Abstract
{
    public interface IStPiusRepository : IGenericRepository<StPiusStudents>
    {
       
    }
}
