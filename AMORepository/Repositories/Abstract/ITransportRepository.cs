﻿using AMORepository.Entity;
using AMORepository.Generic;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMORepository.Repositories.Abstract
{
    public interface ITransportRepository : IGenericRepository<Transport>
    {
    }
}
