﻿using System;
using System.Threading.Tasks;
using AMORepository.Entity;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Linq;
using AMORepository.Repositories.Abstract;

namespace AMORepository.Repositories.Concrete
{
   
    public class ExpenseRepository : IExpenseRepository
    {
        private readonly IConfiguration configuration;

        public ExpenseRepository(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public async Task<IEnumerable<Expense>> GetAllAsync()
        {
            var sql = "SELECT * FROM Expense";
            using (var connection = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {
                connection.Open();
                var result = await connection.QueryAsync<Expense>(sql);
                return result.ToList();
            }
        }

        public async Task<int> AddAsync(Expense expense)
        {

            var sql = "Insert into Expense (Id,CreatedBy,CreatedDate,Amount, ExpenseDetails, ApprovedBy) VALUES (@Id,@CreatedBy,@CreatedDate,@Amount, @ExpenseDetails, @ApprovedBy)";
            using (var connection = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {
                connection.Open();
                var result = await connection.ExecuteAsync(sql, expense);
                return result;
            }
        }

        public Task<Expense> GetByIdAsync(long id)
        {
            throw new NotImplementedException();
        }

        public Task<int> UpdateAsync(Expense entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteAsync(Guid id)
        {
            throw new NotImplementedException();
        }
    }
}
