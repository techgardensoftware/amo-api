﻿using AMORepository.Entity;
using AMORepository.Repositories.Abstract;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMORepository.Repositories.Concrete
{
    public class TransportRepository : ITransportRepository
    {
        private readonly IConfiguration configuration;
        public TransportRepository(IConfiguration configuration)
        {
            this.configuration = configuration;
        }
        public async Task<IEnumerable<Transport>> GetAllAsync()
        {
            var sql = "SELECT * FROM Transport";
            using (var connection = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {
                connection.Open();
                var result = await connection.QueryAsync<Transport>(sql);
                return result.ToList();
            }
        }
        public async Task<int> AddAsync(Transport transport)
        {
            var sql = "Insert into Transport (Id,ModeOfTransport,AppliedDate,MinAmount,Distance,Status) VALUES (@Id,@ModeOfTransport,@AppliedDate,@MinAmount,@Distance,@Status)";
            using (var connection = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {
                connection.Open();
                var result = await connection.ExecuteAsync(sql, transport);
                return result;
            }
        }
        public Task<Transport> GetByIdAsync(long id)
        {
            throw new NotImplementedException();
        }
        public Task<int> UpdateAsync(Transport entity)
        {
            throw new NotImplementedException();
        }
        public Task<int> DeleteAsync(Guid id)
        {
            throw new NotImplementedException();
        }
    }
}
