﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using AMORepository.Entity;
using AMORepository.Generic;
using AMORepository.Repositories.Abstract;
using Dapper;
using Microsoft.Extensions.Configuration;

namespace AMORepository.Repositories.Concrete
{
    public class StudentRepository : IStudentRepository
    {
        private readonly IConfiguration configuration;

        public StudentRepository(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public async Task<IEnumerable<Student>> GetAllAsync()
        {
            var sql = "SELECT * FROM Student";
            using (var connection = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {
                connection.Open();
                var result = await connection.QueryAsync<Student>(sql);
                return result.ToList();
            }
        }

        public  Task<int> AddAsync(Student student)
        {
            throw new NotImplementedException();
        }

        public Task<Expense> GetByIdAsync(long id)
        {
            throw new NotImplementedException();
        }

        public Task<int> UpdateAsync(Expense entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteAsync(Guid id)
        {
            throw new NotImplementedException();
        }

        Task<Student> IGenericRepository<Student>.GetByIdAsync(long id)
        {
            throw new NotImplementedException();
        }

        public Task<int> UpdateAsync(Student entity)
        {
            throw new NotImplementedException();
        }
    }
}
