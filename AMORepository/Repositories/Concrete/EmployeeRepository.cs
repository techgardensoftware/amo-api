﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using AMORepository.Entity;
using Dapper;
using Microsoft.Extensions.Configuration;

namespace AMORepository.Repositories.Concrete
{
    public class EmployeeRepository
    {
        // my comment in master branch
        private readonly IConfiguration configuration;

        public EmployeeRepository(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public async Task<IEnumerable<Employee>> GetAllAsync()
        {
            var sql = "SELECT * FROM Employee";
            using (var connection = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {
                connection.Open();
                var result = await connection.QueryAsync<Employee>(sql);
                return result.ToList();
            }
        }

        public async Task<int> AddAsync(Employee expense)
        {
            var sql = "Insert into Employee (Id,FirstName,MiddleName,LastName, DOJ, Address, AdharNumber,PanNumber" +
                ") VALUES (@Id,@FirstName,@MiddleName,@LastName, @DOJ, @Address, @AdharNumber,@PanNumber)";
            using (var connection = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {
                connection.Open();
                var result = await connection.ExecuteAsync(sql, expense);
                return result;
            }
        }

        public Task<Expense> GetByIdAsync(long id)
        {
            throw new NotImplementedException();
        }

        public Task<int> UpdateAsync(Expense entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteAsync(Guid id)
        {
            throw new NotImplementedException();
        }
    }
}
