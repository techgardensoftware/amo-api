﻿using System;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using AMORepository.Entity;
using AMORepository.Generic;
using AMORepository.Repositories.Abstract;
using Dapper;
using Microsoft.Extensions.Configuration;

namespace AMORepository.Repositories.Concrete
{
    public class StPiusRepository : IStPiusRepository
    {
        private readonly IConfiguration _configuration;
        public StPiusRepository(IConfiguration configuration)
        {
            this._configuration = configuration;
        }

        public async Task<System.Collections.Generic.IEnumerable<StPiusStudents>> GetAllAsync()
        {
            var sql = "SELECT * FROM StPiusStudents";
            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                connection.Open();
                var result = await connection.QueryAsync<StPiusStudents>(sql);
                return result.ToList();
            }
        }


        public Task<int> AddAsync(StPiusStudents entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteAsync(Guid id)
        {
            throw new NotImplementedException();
        }


        public Task<int> UpdateAsync(StPiusStudents entity)
        {
            throw new NotImplementedException();
        }

        Task<StPiusStudents> IGenericRepository<StPiusStudents>.GetByIdAsync(long id)
        {
            throw new NotImplementedException();
        }
    }
}
