﻿using AMORepository.Entity;
using AMORepository.Repositories.Abstract;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMORepository.Repositories.Concrete
{
    public class InvoiceRepository : IInvoiceRepository
    {
        private readonly IConfiguration configuration;

        public InvoiceRepository(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public async Task<IEnumerable<Invoice>> GetAllAsync()
        {
            var sql = "SELECT * FROM Invoice";
            var conStr = configuration.GetConnectionString("DefaultConnection");
            using (var connection = new SqlConnection(conStr)) 
            {
                connection.Open();
                var result = await connection.QueryAsync<Invoice>(sql);
                
                return result.ToList();
            }
        }
        public async Task<int> AddAsync(Invoice invoice)
        {
            var sql = "Insert into Invoice (Id,InvoiceDate,InvoiceAmmount ,CreatedBy, InvoiceTo,Statuss,ApprovedBy, Remark) VALUES (@Id,@InvoiceDate,@InvoiceAmmount ,@CreatedBy, @InvoiceTo,@Status,@ApprovedBy, @Remark)";
            using (var connection = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {
                connection.Open();
                var result = await connection.ExecuteAsync(sql, invoice);
                return result;
            }
        }
      
  

        public Task<int> DeleteAsync(Guid id)
        {
            throw new NotImplementedException();
        }

       
        public Task<Invoice> GetByIdAsync(long id)
        {
            throw new NotImplementedException();
        }

        public Task<int> UpdateAsync(Invoice entity)
        {
            throw new NotImplementedException();
        }
    }
}
