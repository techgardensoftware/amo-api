﻿using System;
using System.Threading.Tasks;
using AMORepository.Entity;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Linq;
using AMORepository.Repositories.Abstract;
using AMORepository.Generic;

namespace AMORepository.Repositories.Concrete
{
   
    public class VehicleRepository : IVehicleRepository
    {
        private readonly IConfiguration configuration;

        public VehicleRepository(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public async Task<IEnumerable<Vehicle>> GetAllAsync()
        {
            var sql = "SELECT * FROM Vehicle";
            using (var connection = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {
                connection.Open();
                var result = await connection.QueryAsync<Vehicle>(sql);
                return result.ToList();
            }
        }

        public  Task<int> AddAsync(Vehicle vehicle)
        {

          throw new NotImplementedException();
        }

        public Task<Vehicle> GetByIdAsync(long id)
        {
            throw new NotImplementedException();
        }

        public Task<int> UpdateAsync(Vehicle entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteAsync(Guid id)
        {
            throw new NotImplementedException();
        }

        
    }
}
