﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AMORepository.Generic
{
    public interface IGenericRepository<T> where T : class 
    {
        Task<T> GetByIdAsync(long id);
        Task<IEnumerable<T>> GetAllAsync();
        Task<int> AddAsync(T entity);
        Task<int> UpdateAsync(T entity);
        Task<int> DeleteAsync(Guid id);
    }
}
