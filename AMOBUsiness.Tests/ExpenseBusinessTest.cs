﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AMOBusiness.Abstract;
using AMOBusiness.Concrete;
using AMOBusiness.Dto;
using AMORepository.Entity;
using AMORepository.Repositories.Abstract;
using Moq;
using NUnit.Framework;

namespace AMOBUsiness.Tests
{
    [TestFixture]
    public class ExpenseBusinessTest
    {

        //Some updation is made + another change

        private Mock<IExpenseRepository> _expenseRepositoryMock;
        private IExpenseBusiness _expenseBusiness;
      
        public ExpenseBusinessTest()
        {
            _expenseRepositoryMock = new Mock<IExpenseRepository>();
            _expenseBusiness = new ExpenseBusiness(_expenseRepositoryMock.Object) ;
        }

        [SetUp]
        public void Setup()
        {
         
        }

        [Test]
        public async Task GetExpensesAsyncShouldBeSuccessful()
        {
            //Arrange
            var expenseResultValue = CreateExpenseMock();

            _expenseRepositoryMock.Setup(x => x.GetAllAsync()).ReturnsAsync(expenseResultValue);

            //Act
            var expenseResult = await _expenseBusiness.GetExpensesAsync();

            //Assess
            _expenseRepositoryMock.Verify(x => x.GetAllAsync(), Times.Once);
            Assert.AreEqual(expenseResult.FirstOrDefault().Id, expenseResultValue.FirstOrDefault().Id);
         }

        [Test]
        public async Task CreateExpensesAsyncReturnsZeroWhenAmountIsZero()
        {
            //Arrange
            var expenseDto = new ExpenseDto()
            {
                Id = 300,
                Amount = 0,
                ApprovedBy = "Rijesh",
                CreatedBy = "Anil",
                CreatedDate =
                System.DateTime.Now,
                ExpenseDetails = "sample data"
            };


            _expenseRepositoryMock.Setup(x => x.AddAsync(It.IsAny<Expense>()).Result).Returns(1);
            var expenctedResult = 1;


            //Act
            var expenseResult = await _expenseBusiness.CreateExpenseAsync(expenseDto);

            //Assert
            _expenseRepositoryMock.Verify(x => x.AddAsync(It.IsAny<Expense>()), Times.Never);
            Assert.AreNotEqual(expenctedResult, expenseResult);
        }


        [Test]
        public async Task WhyUnitTestImp()
        {
            //Arrange
            var expenseDto = new ExpenseDto()
            {
                Id = 300,
                Amount = 0,
                ApprovedBy = "Rijesh",
                CreatedBy = "Anil",
                CreatedDate =
                System.DateTime.Now,
                ExpenseDetails = "sample data"
            };


            _expenseRepositoryMock.Setup(x => x.AddAsync(It.IsAny<Expense>()).Result).Returns(1);
            var expenctedResult = 1;


            //Act
            var expenseResult = await _expenseBusiness.CreateExpenseAsync(expenseDto);

            //Assert
            _expenseRepositoryMock.Verify(x => x.AddAsync(It.IsAny<Expense>()), Times.Once);
            Assert.AreEqual(expenctedResult, expenseResult);
        }

        [Test]
        public async Task CreateExpensesAsyncShouldBeSuccessfulWhenAmountIsNonZero()
        {
            //Arrange
            var expenseDto = new ExpenseDto()
            {
                Id = 300,
                Amount = 100,
                ApprovedBy = "Rijesh",
                CreatedBy = "Anil",
                CreatedDate =
                System.DateTime.Now,
                ExpenseDetails = "sample data"
            };


            _expenseRepositoryMock.Setup(x => x.AddAsync(It.IsAny<Expense>()).Result).Returns(1);
            var expenctedResult = 1;


            //Act
            var expenseResult = await _expenseBusiness.CreateExpenseAsync(expenseDto);

            //Assert
            _expenseRepositoryMock.Verify(x => x.AddAsync(It.IsAny<Expense>()), Times.Once);
            Assert.AreEqual(expenctedResult, expenseResult);
        }

        

        private IEnumerable<Expense> CreateExpenseMock()
        {
            return new List<Expense>{
                new Expense()
                {
                    Id = 100,
                    Amount = 300,
                    CreatedBy = "testuser",
                    ApprovedBy = "testapproveduser",
                    CreatedDate = System.DateTime.Now,
                    ExpenseDetails = "test expense details"
                }
            };
        }
    }
}
