﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AMOBusiness.Abstract;
using AMOBusiness.Dto;
using AMOPortal.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace AMOPortal.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class ExpenseController : ControllerBase
    {
        private readonly IExpenseBusiness _expenseBusiness;
        private readonly ILogger<ExpenseController> _logger;

        public ExpenseController(IExpenseBusiness expenseBusiness, ILogger<ExpenseController> logger)
        {
            _expenseBusiness = expenseBusiness;
            _logger = logger;
            _logger.LogInformation("Constructed expense controller");
        }

        [HttpGet]
        public async Task<IEnumerable<ExpenseDto>> Get()
        {
            try
            {
                return await _expenseBusiness.GetExpensesAsync();
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.ToString());
                return new List<ExpenseDto> { new ExpenseDto { Id = 100, Amount = 300 } };
            }
        }

        [HttpPost]
        public async Task Post([FromBody] ExpenseDto expenseDto)
        {
            
            try
            {
                
                var result =  await _expenseBusiness.CreateExpenseAsync(expenseDto);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException
                {
                    Status = 500,
                   Value = ex
                };
            }
            
        }
    }
}
