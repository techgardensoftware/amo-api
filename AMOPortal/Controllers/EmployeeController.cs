﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AMOBusiness.Abstract;
using AMOBusiness.Dto;
using AMOPortal.DataModel;
using AMOPortal.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace AMOPortal.Controllers
{
    // https://localhost:5001/api/Employee 

    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly IEmployeeBusiness _employeeBusiness;
        private readonly ILogger<EmployeeController> _logger;

        public EmployeeController(IEmployeeBusiness employeeBusiness, ILogger<EmployeeController> logger)
        {
            _employeeBusiness = employeeBusiness;
            _logger = logger;
            _logger.LogInformation("Constructed employee controller");
        }

        //[HttpGet]
        //public async ActionResult<IAsyncEnumerable<EmployeeDto>> Get()
        //{
        //    try
        //    {
        //        var employees = await _employeeBusiness.GetEmployeeAsync();
        //        return Ok(employees);
        //    }
        //    catch (Exception ex)
        //    {
        //        return StatusCode(500, "Internal server error");
        //    }
        //}

        //[HttpPost]
        //public async Task<Employee> Get()
        //{
        //    try
        //    {

        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //}
    }
}
