﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AMOBusiness.Abstract;
using AMOBusiness.Dto;
using AMOPortal.DataModel;
using AMOPortal.Exceptions;
using Microsoft.AspNetCore.Mvc;

namespace AMOPortal.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        private readonly IStudentBusiness _studentBusiness;

        public StudentController(IStudentBusiness studentBusiness)
        {
            _studentBusiness = studentBusiness;
        }

        [HttpGet]
        public async Task<IEnumerable<StudentDto>> Get()
        {
            try
            {
                return await _studentBusiness.GetStudentsAsync();
            }
            catch(Exception ex)
            {
                throw new HttpResponseException
                {
                    Status = 500,
                    Value = ex
                };
            }
        }
    }
}
