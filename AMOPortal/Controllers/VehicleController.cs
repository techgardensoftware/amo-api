﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AMOBusiness.Abstract;
using AMOBusiness.Dto;
using AMOPortal.Exceptions;
using Microsoft.AspNetCore.Mvc;

namespace AMOPortal.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class VehicleController : ControllerBase
    {
        private readonly IVehicleBusiness _vehicleBusiness;
       
        public VehicleController(IVehicleBusiness vehicleBusiness)
        {
            _vehicleBusiness = vehicleBusiness;
        }

        [HttpGet]
        public async Task<IEnumerable<VehicleDto>> Get()
        {
            try
            {
                return await _vehicleBusiness.GetVehicleAsync();
            }
            catch (Exception ex)
            {
                throw new HttpResponseException
                {
                    Status = 500,
                    Value = ex
                };

            }
                  
        }

      
    }
}
