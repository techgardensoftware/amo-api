﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AMOBusiness.Abstract;
using AMOBusiness.Dto;
using Microsoft.AspNetCore.Mvc;

namespace AMOPortal.Controllers
{
    // https://localhost:5001/api/stpius

    [Route("api/[controller]")]
    [ApiController]
    public class StPiusController : ControllerBase
    {
        private readonly IStPiusBusiness _stPiusBusiness;
       

        public StPiusController(IStPiusBusiness stPiusBusiness)
        {
            _stPiusBusiness = stPiusBusiness;
          
        }

        [HttpGet]
       public async Task<IEnumerable<StPiusDto>> Get()
        {
            try
            {
                return await _stPiusBusiness.GetStPiusStudentsAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        // Post -- Adding new resource/item to the server
        // Put -- Adding/Updating a new resource/item
        // Delete -- Delete a resource
    }
}
