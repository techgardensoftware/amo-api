﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AMOBusiness.Abstract;
using AMOBusiness.Dto;
using Microsoft.AspNetCore.Mvc;

namespace AMOPortal.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class InvoiceController : Controller
    {
       
        private readonly IInvoiceBusiness _invoiceBusiness;
        public InvoiceController(IInvoiceBusiness invoiceBusiness)
        {
            _invoiceBusiness = invoiceBusiness;
        }

        [HttpGet]
        public async Task<IEnumerable<InvoiceDto>> Get()
        {
            return await _invoiceBusiness.GetInvoiceAsync();
        }

        [HttpPost]
        public async Task Post([FromBody] InvoiceDto invoiceDto)
        {
            invoiceDto.InvoiceDate = DateTime.Now;
            var result = await _invoiceBusiness.CreateInvoiceAsync(invoiceDto);
        }
    }
    
}
