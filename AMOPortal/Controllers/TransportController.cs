﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AMOBusiness.Abstract;
using AMOBusiness.Dto;
using Microsoft.AspNetCore.Mvc;

namespace AMOPortal.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class TransportController : ControllerBase
    {
        private readonly ITransportBusiness _transportBusiness;
       
        public TransportController(ITransportBusiness transportBusiness)
        {
            _transportBusiness = transportBusiness;
        }

        [HttpGet]
        public async Task<IEnumerable<TransportDto>> Get()
        {
            return await _transportBusiness.GetTransportAsync();      
        }

        [HttpPost]
        public async Task Post([FromBody] TransportDto transportDto)
        {
            transportDto.AppliedDate = DateTime.Now;
            var result = await _transportBusiness.CreateTransportAsync(transportDto);

        }
    }
}
