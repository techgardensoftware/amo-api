﻿using System;
namespace AMOPortal.DataModel
{
    public class Employee
    {
        public string EmployeeName { get; set; }
        public DateTime JoiningDate { get; set; }
        public string Department{ get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
    }
}
