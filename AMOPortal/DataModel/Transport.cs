﻿using System;
namespace AMOPortal.DataModel
{
    public class Transport
    {
        public string ModeOfTransport { get; set; }

        public DateTime AppliedDate { get; set; }

        public decimal MinAmount { get; set; }

        public double Distance { get; set; }

        public string Status { get; set; }
    }
}