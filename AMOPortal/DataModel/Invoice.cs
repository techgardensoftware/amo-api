﻿using System;

namespace AMOPortal.DataModel
{
    public class Invoice
    {
        public int Id { get; set; }
        public DateTime InvoiceDate { get; set; }
        public decimal InvoiceAmmount { get; set; }
        public string CreatedBy { get; set; }
        public string InvoiceTo { get; set; }
        public bool Status { get; set; }
        public string ApprovedBy { get; set; }
        public string Remark { get; set; }
    }
}
