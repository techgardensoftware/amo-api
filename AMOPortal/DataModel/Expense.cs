﻿using System;
namespace AMOPortal.DataModel
{
    public class Expense
    {
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public decimal Amount { get; set; }

        public string ExpenseDetails { get; set; }

        public string ApprovedBy { get; set; }
    }
}
