﻿using AMOBusiness.Abstract;
using AMOBusiness.Concrete;
using AMORepository.Repositories.Abstract;
using AMORepository.Repositories.Concrete;
using Microsoft.Extensions.DependencyInjection;

namespace AMOPortal
{
    public static class ServiceRegistration
    {
        // Dependecy Injection

        public static void AddInfrastructure(this IServiceCollection services)
        {
            //Registration of Business Layer dependencies

            //you have to register your dependencies here
            services.AddTransient<IStPiusBusiness, StPiusBusiness>();
            services.AddTransient<IStPiusRepository, StPiusRepository>();


            services.AddTransient<IExpenseBusiness, ExpenseBusiness>();
            

            services.AddTransient<IStudentBusiness, StudentBusiness>();

            services.AddTransient<IVehicleBusiness, VehicleBusiness>();

            services.AddTransient<ITransportBusiness, TransportBusiness>();

            //Registration of Repository Layer dependencies
            services.AddTransient<IExpenseRepository, ExpenseRepository>();
            

            services.AddTransient<IVehicleRepository, VehicleRepository>();

            services.AddTransient<ITransportRepository, TransportRepository>();

            services.AddTransient<IStudentRepository, StudentRepository>();

            // Registration of Business Layer dependencies
            services.AddTransient<IInvoiceBusiness, InvoiceBusiness>();

            //Registration of Repository Layer dependencies
            services.AddTransient<IInvoiceRepository, InvoiceRepository>();
            
        }
    }
}
