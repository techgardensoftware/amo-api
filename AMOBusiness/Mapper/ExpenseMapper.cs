﻿using System.Collections.Generic;
using AMOBusiness.Dto;
using AMORepository.Entity;

namespace AMOBusiness.Mapper
{
    public static class ExpenseMapper
    {
        public static ExpenseDto ToDto(this Expense expense)
        {
            return new ExpenseDto
            {
                Id = expense.Id,
                Amount = expense.Amount,
                ApprovedBy = expense.ApprovedBy,
                CreatedBy = expense.CreatedBy,
                CreatedDate = expense.CreatedDate,
                ExpenseDetails = expense.ExpenseDetails
            };
        }

        public static IReadOnlyList<ExpenseDto> ToDtoList(this IEnumerable<Expense> expenses)
        {
            if (expenses != null)
            {
                var expenseDtoList = new List<ExpenseDto>();

                foreach (var expense in expenses)
                {
                    expenseDtoList.Add(expense.ToDto());
                }
                return expenseDtoList;
            }
            return null;
        }

        public static Expense ToEntity(this ExpenseDto expense)
        {
            if (expense != null)
            {
                return new Expense
                {
                    Id = expense.Id,
                    Amount = expense.Amount,
                    ApprovedBy = expense.ApprovedBy,
                    CreatedBy = expense.CreatedBy,
                    CreatedDate = expense.CreatedDate,
                    ExpenseDetails = expense.ExpenseDetails
                };
            }

            return null;
        }
    }
}
