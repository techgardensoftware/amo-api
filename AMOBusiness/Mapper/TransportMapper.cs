﻿using System.Collections.Generic;
using AMOBusiness.Dto;
using AMORepository.Entity;

namespace AMOBusiness.Mapper
{
    public static class TransportMapper
    {
        public static TransportDto ToDto(this Transport transport)
        {
            return new TransportDto
            {
                Id = transport.Id,
                ModeOfTransport = transport.ModeOfTransport,
                AppliedDate = transport.AppliedDate,
                Distance = transport.Distance,
                MinAmount = transport.MinAmount,
                Status = transport.Status
            };
        }

        public static IReadOnlyList<TransportDto> ToDtoList(this IEnumerable<Transport> transports)
        {
            if (transports != null)
            {
                var transportDtoList = new List<TransportDto>();

                foreach (var transport in transports)
                {
                    transportDtoList.Add(transport.ToDto());
                }
                return transportDtoList;
            }
            return null;
        }

        public static Transport ToEntity(this TransportDto transport)
        {
            if (transport != null)
            {
                return new Transport
                {
                    Id = transport.Id,
                    ModeOfTransport = transport.ModeOfTransport,
                    AppliedDate = transport.AppliedDate,
                    Distance = transport.Distance,
                    MinAmount = transport.MinAmount,
                    Status = transport.Status
                };
            }

            return null;
        }
    }
}
