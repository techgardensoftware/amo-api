﻿using AMOBusiness.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using AMORepository.Entity;

namespace AMOBusiness.Mapper
    
{
    public static class InvoiceMapper
    {
        public static InvoiceDto ToDto(this Invoice invoice)
        {
            return new InvoiceDto
            {
                Id = invoice.Id,
                InvoiceDate = invoice.InvoiceDate,
                InvoiceAmmount=invoice.InvoiceAmmount,
                CreatedBy=invoice.CreatedBy,
                InvoiceTo=invoice.InvoiceTo,
                Status=invoice.Status,
                ApprovedBy=invoice.ApprovedBy,
                Remark=invoice.Remark
                
            };
            
    }

        public static IReadOnlyList<InvoiceDto> ToDtoList(this IEnumerable<Invoice> invoices)
        {
            if (invoices != null)
            {
                var invoiceDtoList = new List<InvoiceDto>();

                foreach (var invoice in invoices)
                {
                    invoiceDtoList.Add(invoice.ToDto());
                }
                return invoiceDtoList;
            }
            return null;
        }

        public static Invoice ToEntity(this InvoiceDto invoice)
        {
            if (invoice != null)
            {
                return new Invoice
                {
                    Id = invoice.Id,
                    InvoiceDate = invoice.InvoiceDate,
                    InvoiceAmmount = invoice.InvoiceAmmount,
                    CreatedBy = invoice.CreatedBy,
                    InvoiceTo = invoice.InvoiceTo,
                    Status = invoice.Status,
                    ApprovedBy = invoice.ApprovedBy,
                    Remark = invoice.Remark
                };
            }

            return null;
        }
    }
}


                      