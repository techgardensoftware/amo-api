﻿using System;
using System.Collections.Generic;
using AMOBusiness.Dto;
using AMORepository.Entity;

namespace AMOBusiness.Mapper
{
    public static class StPiusMapper
    {
        public static StPiusDto ToDto(this StPiusStudents stpiusStudents)
        {
            return new StPiusDto
            {
                Id = stpiusStudents.Id,
                Name = stpiusStudents.Name,
                Address = stpiusStudents.Address,
                DateOfGraduation = stpiusStudents.DateOfGraduation
                
            };
        }

        public static IReadOnlyList<StPiusDto> ToDtoList(this IEnumerable<StPiusStudents> stpiusStudents)
        {
            if (stpiusStudents != null)
            {
                var stPiusDtoList = new List<StPiusDto>();

                foreach (var stpiusStudent in stpiusStudents)
                {
                    stPiusDtoList.Add(stpiusStudent.ToDto());
                }
                return stPiusDtoList;
            }
            return null;
        }
    }
}
