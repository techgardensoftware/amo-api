﻿using System.Collections.Generic;
using AMOBusiness.Dto;
using AMORepository.Entity;

namespace AMOBusiness.Mapper
{
    public static class VehicelMapper
    {
        public static VehicleDto ToDto(this Vehicle vehicle)
        {
            return new VehicleDto
            {
                Id = vehicle.Id,
                Name = vehicle.Name,
                Class = vehicle.Class,
                CreateDate = vehicle.CreateDate,

            };
        }

        public static IReadOnlyList<VehicleDto> ToDtoList(this IEnumerable<Vehicle> vehicles)
        {
            if (vehicles != null)
            {
                var vehicleDtoList = new List<VehicleDto>();

                foreach (var vehicle in vehicles)
                {
                    vehicleDtoList.Add(vehicle.ToDto());
                }
                return vehicleDtoList;
            }
            return null;
        }

        //public static Vehicle ToEntity(this VehicleDto vehicle)
        //{
        //    if (vehicle != null)
        //    {
        //        return new Vehicle
        //        {
        //            Id = vehicle.Id,
        //            Name = vehicle.Name,
        //            Class = vehicle.Class,
        //            CreateDate = vehicle.CreateDate,

        //        };
        //    }

        //    return null;
        //}
    }
}
