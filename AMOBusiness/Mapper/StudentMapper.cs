﻿using System;
using System.Collections.Generic;
using AMOBusiness.Dto;
using AMORepository.Entity;

namespace AMOBusiness.Mapper
{
    public static class StudentMapper
    {
        public static StudentDto ToDto(this Student student)
        {
            return new StudentDto
            {
                Id = student.Id,
                Name = student.Name,
                Class = student.Class,
                CreatedDate = student.CreatedDate
            };
        }

        public static IReadOnlyList<StudentDto> ToDtoList(this IEnumerable<Student> students)
        {
            if (students != null)
            {
                var studentDtoList = new List<StudentDto>();

                foreach (var student in students)
                {
                    studentDtoList.Add(student.ToDto());
                }
                return studentDtoList;
            }
            return null;
        }

        //public static Expense ToEntity(this ExpenseDto expense)
        //{
        //    if (expense != null)
        //    {
        //        return new Expense
        //        {
        //            Id = expense.Id,
        //            Amount = expense.Amount,
        //            ApprovedBy = expense.ApprovedBy,
        //            CreatedBy = expense.CreatedBy,
        //            CreatedDate = expense.CreatedDate,
        //            ExpenseDetails = expense.ExpenseDetails
        //        };
        //    }

        //    return null;
        //}
    }
}
