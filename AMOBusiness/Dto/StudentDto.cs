﻿using System;
namespace AMOBusiness.Dto
{
    public class StudentDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Class { get; set; } 
        public DateTime CreatedDate { get; set; }
    }
}
