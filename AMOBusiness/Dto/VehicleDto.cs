﻿using System;
namespace AMOBusiness.Dto
{
    public class VehicleDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Class { get; set; }
       
        public DateTime CreateDate { get; set; }
    }
}
