﻿
namespace AMOBusiness.Dto
{
    public class StPiusDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public object Address { get; set; }
        public object DateOfGraduation { get; set; }     
    }
}
