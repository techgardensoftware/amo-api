﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AMOBusiness.Abstract;
using AMOBusiness.Dto;
using AMORepository.Repositories.Abstract;
using AMOBusiness.Mapper;
using AMORepository.Repositories.Concrete;

namespace AMOBusiness.Concrete
{
    public class ExpenseBusiness : IExpenseBusiness 
    {
        private readonly IExpenseRepository _expenseRepository;

        public ExpenseBusiness(IExpenseRepository expenseRepository)
        {
           
            _expenseRepository = expenseRepository;
        }

        public async Task<IEnumerable<ExpenseDto>> GetExpensesAsync()
        {
            var expenseEntity =  await _expenseRepository.GetAllAsync();
            return expenseEntity.ToDtoList();
        }


        public async Task<int> CreateExpenseAsync(ExpenseDto expenseDto)
        {
            if (expenseDto.Amount == 0)
            {
                return 0;
            }
            var expense = expenseDto.ToEntity();
           return await _expenseRepository.AddAsync(expense);
        }

        
    }
}
