﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AMOBusiness.Abstract;
using AMOBusiness.Dto;
using AMORepository.Repositories.Abstract;
using AMOBusiness.Mapper;

namespace AMOBusiness.Concrete
{
    public class StPiusBusiness : IStPiusBusiness
    {
        private readonly IStPiusRepository _stPiusRepository;
        public StPiusBusiness(IStPiusRepository stPiusRepository)
        {
            _stPiusRepository = stPiusRepository;
        }

        public async Task<IEnumerable<StPiusDto>> GetStPiusStudentsAsync()
        {
            //Getting from azure sql db using repository class
            var stPiusEntity = await _stPiusRepository.GetAllAsync();
            return stPiusEntity.ToDtoList();
        }
    }
}
