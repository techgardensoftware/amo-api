﻿using AMOBusiness.Abstract;
using AMOBusiness.Dto;
using AMOBusiness.Mapper;
using AMORepository.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AMOBusiness.Concrete
{
    public class TransportBusiness : ITransportBusiness
    {
        private readonly ITransportRepository _transportRepository;

        public TransportBusiness(ITransportRepository transportRepository)
        {
            _transportRepository = transportRepository;
        }

        public async Task<IEnumerable<TransportDto>> GetTransportAsync()
        {
            var transportEntity = await _transportRepository.GetAllAsync();
            return transportEntity.ToDtoList();
        }

        public async Task<int> CreateTransportAsync(TransportDto transportDto)
        {
            var transport = transportDto.ToEntity();
            return await _transportRepository.AddAsync(transport);
        }
    }
}
