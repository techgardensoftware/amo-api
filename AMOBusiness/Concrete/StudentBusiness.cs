﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AMOBusiness.Abstract;
using AMOBusiness.Dto;
using AMORepository.Repositories.Abstract;
using AMOBusiness.Mapper;
using AMORepository.Repositories.Concrete;

namespace AMOBusiness.Concrete
{
    public class StudentBusiness : IStudentBusiness 
    {
        private readonly IStudentRepository _studentRepository;

        public StudentBusiness(IStudentRepository studentRepository)
        {
            _studentRepository = studentRepository;
        }

        public async Task<IEnumerable<StudentDto>> GetStudentsAsync()
        {
            var studentEntity =  await _studentRepository.GetAllAsync();
            return studentEntity.ToDtoList();
        }   
    }
}
