﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AMOBusiness.Abstract;
using AMOBusiness.Dto;
using AMORepository.Repositories.Abstract;
using AMOBusiness.Mapper;
using AMORepository.Repositories.Concrete;

namespace AMOBusiness.Concrete
{
    public class InvoiceBusiness:IInvoiceBusiness
    {
        private readonly IInvoiceRepository _invoiceRepository;

        public InvoiceBusiness(IInvoiceRepository invoiceRepository)
        {

            _invoiceRepository = invoiceRepository;
        }

        public async Task<IEnumerable<InvoiceDto>> GetInvoiceAsync()
        {
            var invoiceEntity = await _invoiceRepository.GetAllAsync();
            return invoiceEntity.ToDtoList();
        }
        public async Task<int> CreateInvoiceAsync(InvoiceDto invoiceDto)
        {
            var invoice = invoiceDto.ToEntity();
            return await _invoiceRepository.AddAsync(invoice);
        }
    }
}
