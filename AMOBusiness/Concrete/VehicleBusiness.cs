﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AMOBusiness.Abstract;
using AMOBusiness.Dto;
using AMORepository.Repositories.Abstract;
using AMOBusiness.Mapper;
using AMORepository.Repositories.Concrete;

namespace AMOBusiness.Concrete
{
    public class VehicleBusiness : IVehicleBusiness 
    {
        private  readonly IVehicleRepository _vehicleRepository;

        public VehicleBusiness(IVehicleRepository vehicleRepository)
        {
           
            _vehicleRepository = vehicleRepository;
        }

        public async Task<IEnumerable<VehicleDto>> GetVehicleAsync()
        {
            var vehicleEntity =  await _vehicleRepository.GetAllAsync();
            return vehicleEntity.ToDtoList();
        }


       


    }
}
