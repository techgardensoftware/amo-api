﻿using AMOBusiness.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AMOBusiness.Abstract
{
    public interface IInvoiceBusiness
    {
        Task<IEnumerable<InvoiceDto>> GetInvoiceAsync();
        Task<int> CreateInvoiceAsync(InvoiceDto invoiceDto);
    }
}
