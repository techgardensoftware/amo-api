﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AMOBusiness.Dto;

namespace AMOBusiness.Abstract
{
    public interface IVehicleBusiness 
    {
       Task<IEnumerable<VehicleDto>> GetVehicleAsync();

       
    }
}
