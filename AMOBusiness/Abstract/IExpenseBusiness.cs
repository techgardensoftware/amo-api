﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AMOBusiness.Dto;

namespace AMOBusiness.Abstract
{
    public interface IExpenseBusiness 
    {
       Task<IEnumerable<ExpenseDto>> GetExpensesAsync();

        Task<int> CreateExpenseAsync(ExpenseDto expenseDto);
    }
}
