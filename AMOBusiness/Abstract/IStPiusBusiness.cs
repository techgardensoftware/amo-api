﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AMOBusiness.Dto;

namespace AMOBusiness.Abstract
{
    public interface IStPiusBusiness
    {
        Task<IEnumerable<StPiusDto>> GetStPiusStudentsAsync();
    }
}
