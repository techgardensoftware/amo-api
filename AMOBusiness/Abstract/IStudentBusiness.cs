﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AMOBusiness.Dto;

namespace AMOBusiness.Abstract
{
    public interface IStudentBusiness
    {
        Task<IEnumerable<StudentDto>> GetStudentsAsync();

    }
}
