﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AMOBusiness.Dto;

namespace AMOBusiness.Abstract
{
    public interface IEmployeeBusiness
    {
        Task<IEnumerable<EmployeeDto>> GetEmployeeAsync();

        Task<int> CreateExpenseAsync(EmployeeDto expenseDto);
    }
}
