﻿using AMOBusiness.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AMOBusiness.Abstract
{
    public interface ITransportBusiness
    {
        Task<IEnumerable<TransportDto>> GetTransportAsync();

        Task<int> CreateTransportAsync(TransportDto transportDto);
    }
}
